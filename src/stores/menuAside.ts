import { defineStore } from "pinia";
import { ref } from "vue";
import type { Ref } from "vue";
import $http from "@/utils/request";

export const useAsideMenuStore = defineStore("menuAside", () => {
  // 数据：左侧菜单列表，动态获取
  const menuList: Ref<Array<AsideMenuInstance>> = ref([]);

  // 操作：发起网络请求，动态获取左侧菜单列表
  async function acquire() {
    menuList.value = await $http.get("/menus");
  }

  return { menuList, acquire };
});

export interface AsideMenuInstance {
  id: number;
  authName: string;
  path: string;
  order: number | null;
  children: Array<AsideMenuInstance> | null;
}
