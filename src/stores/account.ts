import { defineStore } from "pinia";
import type {
  _ExtractActionsFromSetupStore,
  _ExtractGettersFromSetupStore,
  _ExtractStateFromSetupStore,
  Store,
} from "pinia";
import $http from "@/utils/request";
import { ref } from "vue";
import type { Ref } from "vue";
import { ElMessage } from "element-plus";
import router from "@/router";

export const useAccountStore = defineStore("account", () => {
  // 数据：登录账户信息，含token
  const account: Ref<AccountInstance> = ref(
    sessionStorage.getItem("account")
      ? <AccountInstance>JSON.parse(sessionStorage.getItem("account") || "{}")
      : { email: "", id: 0, mobile: "", rid: 0, token: "", username: "" }
  );

  // 操作：登录
  const doLogin = async (param: any) => {
    const data: AccountInstance = await $http.post("/login", param);
    account.value = { ...data };
  };

  // 操作：退出登录
  const doLogout = async () => {
    account.value = {
      email: "",
      id: 0,
      mobile: "",
      rid: 0,
      token: "",
      username: "",
    };
    sessionStorage.clear();
    localStorage.clear();
    ElMessage.success("已退出登录");
    await router.push("/login");
  };

  return { account, doLogin, doLogout };
});

/**
 * 以下为类型定义
 */
declare interface AccountInstance {
  id: number;
  rid: number;
  username: string;
  email: string;
  mobile: string;
  token: string;
}

type UseAccountInstance = Store<
  "account",
  _ExtractStateFromSetupStore<{
    doLogin: (param: any) => Promise<void>;
    doLogout: () => Promise<void>;
    account: Ref<AccountInstance>;
  }>,
  _ExtractGettersFromSetupStore<{
    doLogin: (param: any) => Promise<void>;
    doLogout: () => Promise<void>;
    account: Ref<AccountInstance>;
  }>,
  _ExtractActionsFromSetupStore<{
    doLogin: (param: any) => Promise<void>;
    doLogout: () => Promise<void>;
    account: Ref<AccountInstance>;
  }>
>;

export type { AccountInstance, UseAccountInstance };
