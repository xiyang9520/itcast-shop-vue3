import { createRouter, createWebHistory } from "vue-router";
import HomeViewO from "../views/HomeViewO.vue";
import LoginView from "../views/LoginView.vue";
import HomeView from "../views/HomeView.vue";
import TheWelcome from "../components/TheWelcome.vue";
import UserList from "../components/user-manage/UserList.vue";
import { useAccountStore } from "@/stores/account";
import type { UseAccountInstance } from "@/stores/account";

// 账户信息存储器（在真正使用时再初始化，否则会报错）
let accountStore: UseAccountInstance | null = null;

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: "/", redirect: "/login" },
    { path: "/login", component: LoginView },
    {
      path: "/home",
      component: HomeView,
      children: [
        { path: "", name: "home", redirect: "/home/welcome" },
        { path: "welcome", component: TheWelcome },
        { path: "users", component: UserList },
        { path: "roles", component: () => import("../views/AboutView.vue") },
      ],
    },
    {
      path: "/views-home",
      name: "views-home",
      component: HomeViewO,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
  ],
});

// 全局路由前置守卫
router.beforeEach((to, form, next) => {
  if (to.path === "/login") {
    next();
  } else {
    if (accountStore === null) accountStore = useAccountStore();
    if (accountStore?.account.token) {
      next();
    } else {
      router.replace("/login").then((r) => r);
    }
  }
});

export default router;
