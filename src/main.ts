import { createApp } from "vue";
import App from "./App.vue";
// vuex
import { createPinia } from "pinia";

// 路由
import router from "./router";

// 封装的axios实例
import http from "@/utils/request";

// 自定义全局样式
import "./assets/css/global.css";

// element-plus 组件和样式
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
// element-plus 图标
import * as ElementPlusIconsVue from "@element-plus/icons-vue";

const app = createApp(App);

app.use(createPinia());
app.use(router);
app.use(ElementPlus);

// 挂载定制的axios实例
app.config.globalProperties.$http = http;

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}

app.mount("#app");
