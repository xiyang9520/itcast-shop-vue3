import axios, { AxiosError } from "axios";
import type { AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";
import { type UseAccountInstance, useAccountStore } from "@/stores/account";
import { ElMessage, ElMessageBox } from "element-plus";
import type { ComponentPublicInstance } from "vue";

// 账户信息存储器（在真正使用时再初始化，否则会报错）
let accountStore: UseAccountInstance | null = null;

// 后端服务器地址
const BASE_URL = "http://127.0.0.1:8888/api/private/v1";

// 响应拦截器中，错误请求处理器（响应状态码不是2xx范围都会触发该函数）
async function errorHandler(error: any) {
  let message = "出错了！请联系管理员！";
  if (error instanceof AxiosError && error.code === "ERR_NETWORK") {
    message = "连接服务器失败，请检查网络是否可用或联系管理员。";
  }
  await ElMessageBox({
    type: "error",
    title: "错误",
    message,
  });
  return Promise.reject(error);
}

// 响应拦截器中，预处理自定义状态码的处理器
async function filterHandler(response: AxiosResponse) {
  const { data: { data, meta: { status } } } = response;
  let { data: { meta: { msg } } } = response;

  if (status >= 200 && status < 300) return data;
  switch (status) {
    case 404:
      msg = "请求的资源不存在！";
      break;
  }
  ElMessage.warning(msg);
  return Promise.reject(new Error(msg));
}

// 目标接口和方法是否需要添加token
function needVerification({ url, method }: { url?: string; method?: string }) {
  // 白名单模式，指定接口的指定请求方法不需要添加token
  const whiteList = [
    // '/login' 接口的 所有 请求都不需要添加token
    { url: "/login", methods: "all" },
    // '/test' 接口的 'get' 请求不需要添加token
    { url: "/test", methods: ["GET"] },
  ];
  for (const item of whiteList) {
    if (item.url === url) {
      if (typeof item.methods === "string") {
        return false;
      } else {
        return !item.methods.includes(method!.toUpperCase());
      }
    }
  }
  return true;
}

// 创建axios实例
const http: AxiosInstance = axios.create({
  baseURL: BASE_URL,
  timeout: 5000,
  validateStatus(status) {
    return status >= 200 && status < 300;
  },
});

// 请求拦截器
http.interceptors.request.use((config: AxiosRequestConfig) => {
  // 在请求发送之前做些什么
  if (config && needVerification(config)) {
    if (accountStore === null) {
      // 在这里调用store函数，因为初始化时pinia并未挂载好
      accountStore = useAccountStore();
    }
    // 添加token
    if (config?.headers) {
      // 直接赋值，ts会报错，可以加@ts-ignore注释也可先判空
      config.headers["Authorization"] = accountStore.account.token;
    }
  }
  return config;
});

// 响应拦截器
http.interceptors.response.use(filterHandler, errorHandler);

export { BASE_URL };
export default http;

// 结合vue的全局属性获得挂载的$http实例
export declare interface VueConfigHttpInstance extends ComponentPublicInstance {
  $http: AxiosInstance;
}
